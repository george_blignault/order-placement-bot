'use strict'

const putIntents = require('./model-building-functions/put-intents.js');
const putSlotTypes = require('./model-building-functions/put-slot-types.js');
const putBotModel = require('./model-building-functions/put-bot-model.js');

module.exports = function(input) {
  const slotTypes = input.slotTypes;  //objArr
  const intents = input.intents;      //objArr
  const botModel = input.botModel;    //obj

  return putSlotTypes(slotTypes)
  .then(() => {
    return putIntents(intents);
  })
  .then(() => {
    return putBotModel(botModel);
  })
  .catch((fromReject) => {
    console.log('testing - catch trig');
    return fromReject;
  })



/*
  return putSlotTypes(slotTypes)
  .then(putIntents(intents))
  .then(putBotModel(botModel))
  .catch(fromReject => {
    console.log('a promise was rejected in dispatch.js');
  })
*/
}
