'use strict'

//put bot name here.
const botName = 'OrderPlacementBot';

//load bot config file
const buildBot = require('./handler');
const botParams = require(`./bot-files/bot-model/${botName}.json`);

var intentParams = [];
var slotTypesParams = [];

//loop through intents names specified in bot model, load intents from file
//for each intent load the needed slotTypes, unless already loaded for another intent
var i, j, temp;
for (i = 0; i < botParams.intents.length; i++) {
  intentParams[i] = require(`./bot-files/intents/${botParams.intents[i].intentName}.json`);

  //loop through slot names in each intent and find custom slot types
  for (j = 0; j < intentParams[i].slots.length; j++) {
    //if its not a amazon slotType, add to slotTypesParams array
    if (intentParams[i].slots[j].slotType.indexOf('AMAZON.') === -1) {
      temp = require(`./bot-files/custom-slot-types/${intentParams[i].slots[j].slotType}.json`);
      //load this slot if it hasn't already been loaded for another intent
      if (!containsObject(temp, slotTypesParams)) {
        slotTypesParams.push(temp);
      }
    }
  }//j
}//i

//construct input event to bot builder code
var input = {};
input.slotTypes = slotTypesParams;
input.intents = intentParams;
input.botModel = botParams;

buildBot(input, null, function(err, data) {
  if (err) console.log(err);
  else console.log('done with buildbot');
})


function containsObject(obj, objArr) {
  var x;
  for (x = 0; x < objArr.length; x++) {
    if (objArr[x] === obj) return true;
    else return false;
  }
}
