'use strict'

var AWS = require('aws-sdk');
AWS.config.update({
  region: 'us-east-1'
});
var lexBuild = new AWS.LexModelBuildingService();

module.exports = (botModel) => {
  return new Promise((resolve, reject) => {
    return getBotFunc(botModel)
    .then(() => {
      console.log('bot has been updated');
      resolve();
    })
    .catch((err) => {
      console.log('catch  triggered in put-bot-model.js');
      console.log(err);
      reject();
    })
  })
}

const putBotFunc = function(botModelParams) {
  return new Promise((resolve, reject) => {
    lexBuild.putBot(botModelParams, (err, data) => {
      if (err) reject(err);
      else resolve(data);
    })
  })
}

const getBotFunc = function(botModel) {
  return new Promise((resolve, reject) => {
    var params = {
      versionOrAlias: '$LATEST'
    };
    params.name = botModel.name;
    lexBuild.getBot(params, function(err, data) {
      if (err) {//if this bot does not exist
        console.log(`the bot ${botModel.name} is being created`);
        resolve(putBotFunc(botModel));
      }
      else {//if bot exists, update it
        console.log(`the bot ${botModel.name} is being updated`);
        botModel.checksum = data.checksum;
        resolve(putBotFunc(botModel));
      }
    })
  })
}
