'use strict'

var AWS = require('aws-sdk');
AWS.config.update({
  region: 'us-east-1'
});
var lexBuild = new AWS.LexModelBuildingService();

module.exports = (slotTypes) => {
  return new Promise((resolve, reject) => {
    var i;
    var promises = [];
    for (i = 0; i < slotTypes.length; i++) {
      promises.push(getSlotTypeFunc(slotTypes[i]));
    }
    Promise.all(promises)
    .then(() => {
      console.log('all slotType operations handled');
      resolve();
    })//then
    .catch((err) => {
      console.log('catch triggered in put-slot-types.js');
      console.log(err);
      reject();
    })//catch
  })
}

const putSlotTypeFunc = function(slotTypeParams) {
  return new Promise((resolve, reject) => {
    lexBuild.putSlotType(slotTypeParams, (err, data) => {
      if (err) reject(err);
      else resolve(data);
    })
  })
}

const getSlotTypeFunc = function(slotType) {
  return new Promise((resolve, reject) => {
    var params = {
      version: '$LATEST'
    };
    params.name = slotType.name;
    lexBuild.getSlotType(params, (err, data) => {
      //if there is no slotType of that name, send request to make one without checksum
      if (err) {
        console.log(`the slot type ${slotType.name} is being added`);
        resolve(putSlotTypeFunc(slotType));
      }
      else {
        var newSlotType = {};
        newSlotType.name = data.name;
        newSlotType.description = data.description;
        newSlotType.enumerationValues = data.enumerationValues;
        //reject if its the same as the existing one
        console.log('old slot');
        console.log(slotType);
        console.log('new slot');
        console.log(newSlotType);
        if (newSlotType === slotType) {
          console.log(`the slot type ${newSlotType.name} has not changed and will not be updated`);
          resolve();
          //if it has changed, pass the checksum along
        } else {
          console.log(`the slot type ${newSlotType.name} has changed and will be updated`);
          slotType.checksum = data.checksum;
          resolve(putSlotTypeFunc(slotType));
        }
      }
    })
  });
}
