'use strict'

var AWS = require('aws-sdk');
AWS.config.update({
  region: 'us-east-1'
});
var lexBuild = new AWS.LexModelBuildingService();

module.exports = (intents) => {
  return new Promise((resolve, reject) => {
    var i;
    var promises = [];
    for (i = 0; i < intents.length; i++) {
      promises.push(getIntentFunc(intents[i]));
    }
    Promise.all(promises)
    .then(() => {
      console.log('all intent operations handled');
      resolve();
    })//then
    .catch((err) => {
      console.log('catch triggered in put-intents.js');
      console.log(err);
      reject();
    })//catch
  })
}

const putIntentFunc = function(intentParams) {
  return new Promise((resolve, reject) => {
    lexBuild.putIntent(intentParams, (err, data) => {
      if (err) reject(err);
      else resolve(data);
    })
  })
}

const getIntentFunc = function(intent) {
  return new Promise((resolve, reject) => {
    var params = {
      version: '$LATEST'
    };
    params.name = intent.name;
    console.log('pickle rick');
    lexBuild.getIntent(params, function(err, data) {
      if (err) {//if there is no intent with that name, make one
        console.log(`the intent ${intent.name} is being added`);
        resolve(putIntentFunc(intent));
      }
      else {//if there is, get the checksum and remake it
        console.log(`the intent ${intent.name} is being updated`);
        intent.checksum = data.checksum;
        resolve(putIntentFunc(intent));
      }
    })
  })
}
