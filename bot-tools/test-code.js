'use strict'

var AWS = require('aws-sdk');
AWS.config.update({
  region: 'us-east-1'
});
var lexBuild = new AWS.LexModelBuildingService();

var slotTypesParams = require('./bot-files/FlowerTypes.json');
var intentParams = require('./bot-files/OrderFlowers.json');
var botParams = require('./bot-files/OrderFlowersBot.json');

var slotTypeName = slotTypesParams.name;
var intentName = intentParams.name;

var rebuildIntent = function(intentParams){
  return new Promise(function(resolve, reject){
    lexBuild.putIntent(params, function(err, data){
      if (err) reject(err);
      else resolve(data)
    })
  });
}

var rebuildSlot = function(slotTypesParams){
  return new Promise(function(resolve, reject){
    lexBuild.putSlotType(slotTypesParams, (err, data) => {
      if (err) reject(err);
      else resolve(data);
    })
  });
}

var getChecksum = function(slotTypeName){
  return new Promise(function(resolve, reject){
    var params = {
      version: '$LATEST'
    };
    params.name = slotTypeName;
    lexBuild.getSlotType(params, (err, data) => {
      if (err) reject(err);
      else resolve(data.checksum);
    })
  });
}

//-------------------------------------------------------- MAIN
//get the checksum of the slotTypesParams
var a = getChecksum(slotTypeName).then(fromResolve => {
  var params = slotTypesParams;
  params.checksum = fromResolve;
  console.log('this is from resolve' + fromResolve);
  return rebuildSlot(params);
}).catch(fromreject => {
    console.log('this is from reject: ' + fromreject);
})



/*
rebuildSlot(slotTypesParams)
.then(fromResolve => {
  console.log(testFunc());
}).catch(fromReject => {
  console.log(fromReject);
})
*/
