'use strict'

/*
* event contains three objects: slotTypes, intents and botModel
*/

const dispatch = require('./dispatch.js');

module.exports = (event, context, callback) => {
    try {
        dispatch(event).then(fromResolve => {
            callback(null, fromResolve);
        });
    } catch (err) {
        callback(err);
    }
};
