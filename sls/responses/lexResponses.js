'use strict';

module.exports.delegate = function(sessionAttributes, slots) {
  if (!sessionAttributes) {
    var newSessionAttributes = null;
  };
  return {
    sessionAttributes: newSessionAttributes,
    dialogAction: {
      type: 'Delegate',
      slots
    }
  };
}
module.exports.elicitSlot = function(sessionAttributes, intentName, slots, slotToElicit, message) {
  if (!sessionAttributes) {
    var newSessionAttributes = null;
  };
  return {
    sessionAttributes: newSessionAttributes,
    dialogAction: {
      type: 'ElicitSlot',
      intentName,
      slots,
      slotToElicit,
      message
    }
  };
}

module.exports.close = function(sessionAttributes, fulfillmentState, message, title, subTitle, imageUrl, buttonUrl) {
  if (title && imageUrl && buttonUrl) {
    return {
      dialogAction: {
        type: 'Close',
        fulfillmentState,
        message,
        responseCard: getResponseCard(title, subTitle, imageUrl, buttonUrl)
      },
    };
  } else {
    return {
      dialogAction: {
        type: 'Close',
        fulfillmentState,
        message
      },
    };
  }
}


function getResponseCard(title, subTitle, imageUrl, buttonUrl) {
  return {
    contentType: 'application/vnd.amazonaws.card.generic',
    genericAttachments: [
      {
        title: title,
        subTitle: subTitle,
        imageUrl: imageUrl,
        buttons: [
          {
            text: "View More Info",
            value: buttonUrl
          }
        ]
      }
    ]
  }
}
