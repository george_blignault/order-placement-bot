'use strict'

const dispatch = require('./dispatch.js');

module.exports.order = (event, context, callback) => {
  try {
    dispatch(event).then(response => {
      callback(null, response);
    });
  } catch (err) {
    callback(err);
  }
};
