'use strict';

const placeOrder = require('./intents/place_order/placeOrder.js');

module.exports = function(intentRequest) {
  console.log(intentRequest);
  const intentName = intentRequest.currentIntent.name;

  if (intentName === 'PlaceOrder') {
    return placeOrder(intentRequest);
  }

  throw new Error(`Intent with name ${intentName} is not supported`);
}
