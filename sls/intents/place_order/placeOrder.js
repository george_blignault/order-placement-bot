'use strict'

const lexResponses = require('../../responses/lexResponses');

module.exports = (intentRequest) => {
  //obtain the current slot population from the lex request
  const slots = intentRequest.currentIntent.slots;
  var productTypes = slots.ProductType;
  var products = slots.Products;
  var startingDate = slots.startingDate;
  var consecutiveOrders = slots.ConsecutiveOrders;
  var productQuantity = slots.ProductQuantity;
  var deliveryMethod = slots.DeliveryMethod;
  var deliveryWindow = slots.DeliveryWindow;
  var deliveryLocation = slots.deliveryLocation;
  console.log(`current slots: ${productTypes} | ${products} | ${startingDate} | ${consecutiveOrders} |
    ${productQuantity} | ${deliveryMethod} | ${deliveryWindow} | ${deliveryLocation}`);

  const source = intentRequest.invocationSource;
  if (source == `DialogCodeHook`) {

    console.log(`DialogCodehook event triggered`);
    return new Promise((resolve, reject) => {
      resolve(lexResponses.delegate(intentRequest.sessionAttributes, slots));
      return;

    });//promise
  } else if (source == `FulfillmentCodeHook`) {

    console.log(`FulfillmentCodeHook event triggered`);
    return new Promise((resolve, reject) => {
      var closeStatement = { message: {
        contentType: 'PlainText',
        content: `closing statement message placeholder`
      }}
      resolve(lexResponses.close(intentRequest.sessionAttributes, 'Fulfilled', closeStatement.message));
      return;
      
    });
  }//else


}
